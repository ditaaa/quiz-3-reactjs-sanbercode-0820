import React from 'react';
import MovieForm from './MovieForm';
import MovieList from './MovieList';
import './MovieEditScreen.css'

export default function MovieEditScreen() {
    return (
        <div id="MovieEditScreen" className="container">
            <MovieList/>
            <hr/>
            <MovieForm/>
        </div>
    )
}