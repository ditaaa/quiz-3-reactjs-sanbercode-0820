import React from 'react';

export default function Footer() {
    return (
        <footer>
            <b>copyright &copy; 2020 by Sanbercode</b>
        </footer>
    )
}