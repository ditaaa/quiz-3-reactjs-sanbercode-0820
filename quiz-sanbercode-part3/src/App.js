  
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import AuthProvider from './assets/components/AuthContext';
import MovieProvider from './assets/components/MovieContext';
import Routes from './Routes';
import './App.css';

export default function App() {
  return (
    <AuthProvider>
      <MovieProvider>
        <BrowserRouter>
          <Routes/>
        </BrowserRouter>
      </MovieProvider>
    </AuthProvider>
  );
}