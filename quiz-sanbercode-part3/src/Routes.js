import React, { useContext } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import './App.css';
import Navbar from './assets/components/Navbar';
import HomeScreen from './assets/components/Home/HomeScreen';
import AboutScreen from './assets/components/About/AboutScreen';
import MovieEditScreen from './assets/components/MovieEdit/MovieEditScreen';
import LoginScreen from './assets/components/Login/LoginScreen';
import { AuthContext } from './assets/components/AuthContext';
import Footer from './assets/components/Footer';

export default function Routes() {
    const { isLoggedIn } = useContext(AuthContext)

    return (
        <div className="App">
            <div id="App-navigation">
                <Navbar />
            </div>
            <div id="App-content">
                <Switch>
                    <Route path="/" exact component={HomeScreen} />
                    <Route path="/about" component={AboutScreen} />
                    <Route path="/movie-edit">
                        {isLoggedIn ? <MovieEditScreen /> : <Redirect to="/" />}
                    </Route>
                    <Route path="/login" component={LoginScreen} />
                </Switch>
            </div>
            <div id="App-footer">
                <Footer />
            </div>
        </div>
    )
}